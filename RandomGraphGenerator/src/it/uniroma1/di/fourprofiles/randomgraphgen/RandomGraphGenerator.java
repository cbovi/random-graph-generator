package it.uniroma1.di.fourprofiles.randomgraphgen;

//Link Grafo Erdos-Renyi
//http://homes.di.unimi.it/~cesabian/WebSAC/erdos-renyi.pdf

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.Random;

public class RandomGraphGenerator {

	public static final int DEBUG=1;
	public static final BigInteger ONE=new BigInteger("1");
	public static final BigInteger TWO=new BigInteger("2");
	public static final String PREFIX_FILE = "/aleatory_graph_";
	
	public static void main(String[] args) throws IOException {
		
		BigInteger vertexes=new BigInteger(args[0]);
		BigInteger totalEdges=vertexes.multiply(vertexes.subtract(ONE)).divide(TWO);
		BigInteger edges=new BigInteger(args[1]);
		
		BigDecimal p_BD=new BigDecimal(edges).divide(new BigDecimal(totalEdges),MathContext.DECIMAL64);
		double p = p_BD.doubleValue();
		
		Random rand=new Random(); // non necessario System.currentTimeMillis()
		//rand.nextInt(10);
		String tab="\t";
		String aCapo="\n";
		String outputFile=args[2]+PREFIX_FILE+args[4]+"_"+vertexes+"_"+p_BD+".txt";
		FileWriter fileWriter = new FileWriter(outputFile);
	    PrintWriter printWriter = new PrintWriter(fileWriter);
	    int countEdges=0;
	    int logLevel=Integer.parseInt(args[3]);
	    
	    for (int i=0;i<vertexes.intValue();i++) {
	    	for (int j=1;j<vertexes.intValue(); j++) { 
	    		if (i<j) {
	    				double coin = rand.nextDouble();
	    				if (logLevel == DEBUG)
	    					System.out.println("coin: "+coin+" - p: "+p);
	    				if (coin < p ) {
	    					countEdges++;
	    					printWriter.print(i);
	    					printWriter.print(tab);
	    					printWriter.print(j);
	    					printWriter.print(aCapo);
	    					if (logLevel == DEBUG)
	    						System.out.println("coin < p : ("+i+","+j+") INSERTED");	    				  
		    			}
	    				else
	    					if (logLevel == DEBUG)
	    						System.out.println("coin >= p : ("+i+","+j+") DISCARDED");	    				  
	    		
	    			
	    	} 
	    	} 
	    } 
	   
	    printWriter.close();
	    
	    BigDecimal density = new BigDecimal(countEdges).divide(new BigDecimal(totalEdges),MathContext.DECIMAL64);
	    System.out.println("\n\nOutput File: "+outputFile);
	    System.out.println("Input Vertexes: "+vertexes);
	    System.out.println("Input Edges: "+edges);
		System.out.println("Total Edges: "+totalEdges);
		System.out.println("Probability: "+p_BD);
		System.out.println("Generated Edges: "+countEdges);
		System.out.println("Discarded Edges: "+(totalEdges.subtract(new BigInteger(""+countEdges))));
		System.out.println("Density: "+density);
		
		String outputFileLog=args[2]+PREFIX_FILE+args[4]+"_"+vertexes+"_"+p_BD+".txt-log";
		FileWriter fileWriterLog = new FileWriter(outputFileLog);
	    PrintWriter printWriterLog = new PrintWriter(fileWriterLog);
	    
	    printWriterLog.println("\n\nOutput File: "+outputFile);
		printWriterLog.println("Input Vertexes: "+vertexes);
		printWriterLog.println("Input Edges: "+edges);
		printWriterLog.println("Total Edges: "+totalEdges);
		printWriterLog.println("Probability: "+p_BD);
		printWriterLog.println("Generated Edges: "+countEdges);
		printWriterLog.println("Discarded Edges: "+(totalEdges.subtract(new BigInteger(""+countEdges))));
		printWriterLog.println("Density: "+density);
		
		printWriterLog.close();
	    
	}
}
